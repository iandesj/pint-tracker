using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PintTracker.Models;
using PintTracker.Models.Context;

namespace PintTracker.Controllers
{
    [Route("api/[controller]")]
    public class KegsController : Controller
    {
        private readonly PintTrackerContext _context;
        public KegsController(PintTrackerContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return new ObjectResult(_context.Kegs.ToList());
        }

        [HttpGet("{id}", Name = "GetKeg")]
        public IActionResult Get(int id)
        {
            var keg = _context.Kegs.SingleOrDefault(x => x.KegId == id);
            if (keg == null)
            {
                return NotFound();
            }
            return new ObjectResult(keg);
        }

        [HttpGet("{id}/Drinks")]
        public IActionResult GetDrinks(int id)
        {
            var keg = _context.Kegs
                .Include(x => x.Drinks)
                .SingleOrDefault(x => x.KegId == id);
            if (keg == null)
            {
                return NotFound();
            }
            return new ObjectResult(keg.Drinks);
        }

        [HttpPost]
        public IActionResult Post([FromBody]Keg keg)
        {
            if (keg == null)
            {
                return BadRequest();
            }

            _context.Kegs.Add(keg);
            _context.SaveChanges();

            return CreatedAtRoute("GetKeg", new { id = keg.KegId }, keg);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Keg kegUpdate)
        {
            if (kegUpdate == null || kegUpdate.KegId != id)
            {
                return BadRequest();
            }
            var keg = _context.Kegs.FirstOrDefault(x => x.KegId == id);
            if (keg == null)
            {
                return NotFound();
            }
            keg.Volume = kegUpdate.Volume;
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var keg = _context.Kegs.FirstOrDefault(x => x.KegId == id);
            if (keg == null)
            {
                return NotFound();
            }
            _context.Kegs.Remove(keg);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
