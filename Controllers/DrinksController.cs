using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PintTracker.Models;
using PintTracker.Models.Context;

namespace PintTracker.Controllers
{
    [Route("api/[controller]")]
    public class DrinksController : Controller
    {
        private readonly PintTrackerContext _context;
        public DrinksController(PintTrackerContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return new ObjectResult(_context.Drinks.ToList());
        }

        [HttpGet("{id}", Name = "GetDrink")]
        public IActionResult Get(int id)
        {
            var  drink = _context.Drinks.SingleOrDefault(x => x.DrinkId == id);
            if (drink == null)
            {
                return NotFound();
            }
            return new ObjectResult(drink);
        }

        [HttpPost]
        public IActionResult Post([FromBody]Drink drink)
        {
            if (drink == null)
            {
                return BadRequest();
            }

            _context.Drinks.Add(drink);
            _context.SaveChanges();

            return CreatedAtRoute("GetDrink", new { id = drink.DrinkId }, drink);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var drink = _context.Drinks.FirstOrDefault(x => x.DrinkId == id);
            if (drink == null)
            {
                return NotFound();
            }
            _context.Drinks.Remove(drink);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
