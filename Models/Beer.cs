using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PintTracker.Models
{
    public class Beer
    {
        public int BeerId { get; set; }
        [Required]
        public string Name { get; set; }
        public Style Style { get; set; }
        public DateTime BrewDate { get; set; }
        public ICollection<Keg> Kegs { get; set; }
    }
}