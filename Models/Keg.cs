using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PintTracker.Models
{
    public class Keg
    {
        public int KegId { get; set; }
        [Required]
        public decimal Volume { get; set; }
        public DateTime Date { get; set; }
        public ICollection<Drink> Drinks { get; set; }
    }
}