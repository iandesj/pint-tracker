using System;
using System.ComponentModel.DataAnnotations;

namespace PintTracker.Models
{
    public class Drink
    {
        public int DrinkId { get; set; }
        [Required]
        public Vessel Vessel { get; set; }
        [Required]
        public Keg Keg { get; set; }
        public DateTime DrinkDate { get; set; }
    }
}