using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace PintTracker.Models.Context
{
    public class PintTrackerContext : DbContext
    {
        public PintTrackerContext(DbContextOptions<PintTrackerContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=pint_tracking.db");
        }

        public DbSet<Vessel> Vessels { get; set; }
        public DbSet<Beer> Beers { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<Keg> Kegs { get; set; }
        public DbSet<Drink> Drinks { get; set; }
    }
}