using System;
using System.ComponentModel.DataAnnotations;

namespace PintTracker.Models
{
    public class Vessel
    {
        public int VesselId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Volume { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}